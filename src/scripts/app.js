/**
 * Created by Philipe on 15/03/2016.
 */
'use strict';

require('babel-polyfill');
const mainController = require('./application/main.controller');
const config = require('./infrastructure/config');
const $ = global.$;

//Document Ready
$( () => {
    config.set( 'mapElement', document.getElementById(config.get('mapId')) );
    mainController.startApplication();
} );