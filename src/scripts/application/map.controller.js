/**
 * Created by Philipe on 14/03/2016.
 */
'use strict';

const GMap = require('../domain/map');
const config = require('../infrastructure/config');
const request = require('../infrastructure/request');
const CustomPromise = require('../infrastructure/customPromise');
const GMapsURL = "https://maps.googleapis.com/maps/api/js?key=" + config.get('GMapApiKey');

function MapController() {
    let promise = new CustomPromise();
    let controls = {
        load: () => promise,
        refresMarkers: refresMarkers,
        goTo: goTo,
        map: {}
    };

    function getCurrentPosition() {
        let promise = new CustomPromise();
        navigator.geolocation.getCurrentPosition( geoPosition => promise.resolve(geoPosition.coords) );
        return promise;
    }

    function loadGoogleMapsScript() {
        return request.getScript(GMapsURL);
    }

    function addMarker(markerData) {
        return controls.map.addMarker(markerData);
    }

    function clearMarkers() {
        return controls.map.clearMarkers();
    }

    function goTo(marker) {
        controls.map.map.panTo(marker.position);
    }

    function refresMarkers(markersData) {
        clearMarkers();
        markersData.forEach( (markerData, idx) => {
            let marker = addMarker(markerData);
            if (idx + 1 === markersData.length) {
                goTo(marker);
            }
        } );
    }

    loadGoogleMapsScript()
        .then( getCurrentPosition )
        .then( coords => {
            controls.map = new GMap( config.get('mapElement'), google, {coords: coords} );
            controls.google = google;
            promise.resolve(controls);
        } )
        .catch( (e) => {
            console.log('Error on loading Google Maps service.', e);
            promise.reject( e => console.log(e) );
        } );

    return controls;
}

module.exports = MapController;