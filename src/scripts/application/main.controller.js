/**
 * Created by Philipe on 14/03/2016.
 */
'use strict';

const MapController = require('./map.controller');
const uiComponents = require('../infrastructure/uiComponents');
const loadSpinner = require('../infrastructure/uiComponents').loadSpinner;
const markersPane = require('../infrastructure/uiComponents').markersPane;
const markersLoader = require('../infrastructure/uiComponents').markersLoader;
const dataTable = require('../infrastructure/uiComponents/dataTable');
const Marker = require('../domain/markers/marker');
const CustomPromise = require('../infrastructure/customPromise');
const csvParser = require('../infrastructure/csvParser');
const config = require('../infrastructure/config');
const $ = global.$;

function loadDependencies() {
    let promise = new CustomPromise();

    const mapController = new MapController();

    uiComponents.preLoad()
        .then( () => {
            mapController
                .load()
                .then( () => promise.resolve(mapController) )
                .catch( e => promise.reject(e) );
        } )
        .catch( e => promise.reject(e) );

    return promise;
}

function startApplication() {

    loadDependencies()
        .then( mapController => {

            $('#manageBtn').on('click', () => markersLoader.show() );

            loadSpinner.show();
            markersPane.listenTo(mapController.map);
            markersLoader
                .on('show', ()=> {
                    let tab = $('#manageTab');
                    if( tab.parent().is('.active') ) tab.trigger('shown.bs.tab');
                    loadSpinner.hide();
                } )
                .on('loadData', obj => {
                    let dataGrid = csvParser(obj.csv, obj.separator);
                    dataGrid.cols.unshift('visible');
                    dataGrid.data = dataGrid.data.map( markerData => {
                        markerData.mapController = mapController;
                        markerData.map = mapController.map.map;
                        let marker = new Marker(markerData, mapController.google);
                        marker.visible = true;
                        return marker;
                    });
                    obj.dataGrid.set(dataGrid);
                    $('.nav-tabs li a:last').tab('show');
                })
                .on('manage', dataGrid => {
                    dataTable(dataGrid.get());
                })
                .on('applyMarkers', markersData => {
                    if (
                        !config.get('markerLabel') ||
                        !config.get('markerLatitude') ||
                        !config.get('markerLongitude')
                    ) {
                        console.log('Please, make sure you have tagged columns for label, latitude and longitude');
                        return;
                    }

                    markersLoader.hide();

                    var dataGrid = markersData.get();
                    dataGrid.data = dataGrid.data.map( marker => {
                        marker.title = marker.markerData[config.get('markerLabel')];

                        marker.setPosition({lat: parseFloat(marker.markerData[config.get('markerLatitude')]), lng: parseFloat(marker.markerData[config.get('markerLongitude')])});
                        if ( !marker.googleMapsMarkerInstance ) marker.generateGoogleMapsMarker();

                        return marker;
                    });
                    markersData.set(dataGrid);

                    markersPane.show();
                    mapController.refresMarkers(markersData.get().data);
                })
                .show();
        } )
        .catch( e => {
            loadSpinner.hide();
            console.log(e);
        } );
}

module.exports = {
    startApplication: startApplication
};