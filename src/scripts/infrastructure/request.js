'use strict';
const CustomPromise = require('../infrastructure/customPromise');
const $ = global.$;

function get(url) {
    var promise = new CustomPromise();

    $.get(url)
        .done( data => promise.resolve(data) )
        .fail( e => promise.reject(e) );

    return promise;
}

function getScript(url) {
    var promise = new CustomPromise();

    $.getScript(url)
        .done( data => promise.resolve(data) )
        .fail( e => promise.reject(e) );

    return promise;
}

module.exports = {
    get: get,
    getScript: getScript
};
