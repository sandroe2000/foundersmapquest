/**
 * Created by Philipe on 15/03/2016.
 */
'use strict';

const template = require('../templateHandler');
const CustomPromise = require('../customPromise');
const $ = global.$;

function markersPane() {
    let promise = new CustomPromise();
    let pane = {
        tpl: {},
        show : () => promise.then( ()=> {
            $('#markersPane').removeClass('off');
            $('#markersList').addClass('in');
        }),
        hide : () => {
            $('#markersPane').addClass('off');
            $('#markersList').removeClass('in');
        },
        listenTo: map => {
            map.on('addMarker', marker => addMarker(marker));
            map.on('clearMarkers', () => clearMarkers() );
            map.on('showMarker', marker => console.log('showMarker', marker));
            map.on('hideMarker', marker => console.log('addMarker', marker));
        }
    };

    function applyBinds() {
        $('#toggleMarkersPane').click( () => {
            $('#markersPane').toggleClass('off');
        } );

        $('#markersList').on('click', '.list-group-item', evt => {
            let el = $(evt.currentTarget);
            let marker = el[0].marker;
            $('#markersList .active').removeClass('active');
            el.addClass('active');
            marker.mapController.goTo(marker);
        } );

        $('#markersList').on('click', '.toggleVisibility', evt => {
            let marker = evt.currentTarget.parentElement.marker;
            toggleMarker(marker, evt.currentTarget);
        } );
    }

    function addMarker(marker) {
        template.get('markersPane.marker', {
                label: marker.title,
                visible: marker.visible ? 'open' : 'close',
                active: marker.visible ? '' : 'inactive'
            })
            .then( tpl => {
                $('#markersList')
                    .append(tpl)
                    .find(tpl)[0]
                    .marker = marker;
            })
            .catch( e => promise.reject(e) );
    }

    function clearMarkers() {
        $('#markersList').empty();
    }

    function toggleMarker(marker, el) {
        let visible = !marker.visible;

        marker.visible = visible;
        marker[visible ? 'show' : 'hide']();
        $(el)
            .children()
            .removeClass('glyphicon-eye-open glyphicon-eye-close')
            .addClass('glyphicon-eye-' + (visible ? 'open' : 'close'))
            .parents('.list-group-item')
            .toggleClass('inactive');
    }

    template.get('markersPane')
        .then( tpl => {
            pane.tpl = $(tpl);
            $('#markersPane').append(pane.tpl);
            applyBinds();
            promise.resolve(pane);
        } )
        .catch( e => {
            console.log(e);
            promise.reject(e);
        } );

    return pane;
}

module.exports = markersPane;