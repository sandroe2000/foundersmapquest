/**
 * Created by Philipe on 22/03/2016.
 */
'use strict';

const template = require('../templateHandler');
const CustomPromise = require('../customPromise');
const $ = global.$;

function loadSpinner() {
    let promise = new CustomPromise();
    let spinner = {
        tpl: {},
        load: load,
        show : () => promise.then(()=>$(spinner.tpl).modal('show')),
        hide : () => $(spinner.tpl).modal('hide')
    };

    function load() {
        template.get('loadSpinner')
            .then( tpl => {
                spinner.tpl = $(tpl);
                $('body').append(spinner.tpl);
                promise.resolve(spinner);
            } )
            .catch( e => {
                console.log(e);
                promise.reject(e);
            } );

        return promise;
    }

    return spinner;
}

module.exports = loadSpinner;