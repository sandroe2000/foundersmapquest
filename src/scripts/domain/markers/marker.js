/**
 * Created by Philipe on 14/03/2016.
 */
'use strict';

const MarkerOptions = require('./marker.options');

class Marker {
    constructor(markerData, google) {

        let events = this.events = {};

        this.mapController = markerData.mapController;
        this.markerData = markerData;
        this.visible = markerData.visible;
        this.map = markerData.map;
        this.google = google;
        this.dispatch = dispatch;

        function dispatch(event) {
            if ( !event || !events[event] ) return;
            events[event].forEach( callback => { if( typeof callback === 'function' ) callback.call(this); });
        }

        return this;
    }

    show() {
        this.googleMapsMarkerInstance.setMap(this.map);
        this.dispatch('show', this);
        return this;
    }

    hide() {
        this.googleMapsMarkerInstance.setMap();
        this.dispatch('hide', this);
        return this;
    }

    setPosition(posObj) {
        if ( !posObj || !posObj.lat || !posObj.lng ) {
            console.log('invalid position object');
            return false;
        }
        this.position = new google.maps.LatLng(posObj);
    }

    generateGoogleMapsMarker() {
        this.googleMapsMarkerInstance = new google.maps.Marker(new MarkerOptions(this));
    }

    on(event, callback) {
        if ( !this.events[event] ) this.events[event] = [];
        this.events[event].push(callback);
    }

    off(event) {
        if ( this.events[event] ) delete this.events[event];
    }
}

module.exports = Marker;