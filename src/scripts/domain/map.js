/**
 * Created by Philipe on 14/03/2016.
 */
'use strict';

class GMap {
    constructor(el, google, options) {
        let events = {};
        let gMapOptions = {
            center: {lat: options.coords.latitude, lng: options.coords.longitude},
            zoom: 12
        };

        this.events = events;
        this.map = new google.maps.Map(el, gMapOptions);
        this.markers = [];
        this.google = google;

        google.maps.event.addListener( this.map, 'tilesloaded', () => this.dispatch('ready') );

        return this;
    }

    dispatch(event, data) {
        if ( !event || !this.events[event] ) return;
        this.events[event].forEach( callback => { if( typeof callback === 'function' ) callback(data); });
    }

    on(event, callback) {
        if ( !this.events[event] ) this.events[event] = [];
        this.events[event].push(callback);
    }

    off(event) {
        if ( this.events[event] ) delete this.events[event];
    }

    addMarker(marker) {
        this.markers.push(marker);
        this.dispatch('addMarker', marker);
        marker.on('showMarker', m => this.dispatch('show', m));
        marker.on('hideMarker', m => this.dispatch('hide', m));
        marker[marker.visible ? 'show' : 'hide']();

        return marker;
    }

    clearMarkers() {
        this.markers.forEach( marker => marker.hide() );
        this.markers.splice(0, this.markers.length - 1);
        this.dispatch('clearMarkers');

        return this;
    }

    removeMarker(data) {

    }
}

module.exports = GMap;