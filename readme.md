# Founders Map Quest - A TryCatch Frontend Developer Test



Tooling:
--------------------------------------

- [Gulp](http://gulpjs.com/)
    Task runner for build, launch

- [Bower](http://bower.io/)
    Package manager for third-party libs 

- [Babel](https://babeljs.io/)
    ES6 to ES1.6 javascript transpiler - The use of this tool is intended to be temporary. By using this, we're enable
to wright ES6 code, without running into browser's support issues, while keeping the code base up to date with the latest
 EcmaScript specifications.
 
- [Webpack](https://babeljs.io/)
 
- [JSHint](https://babeljs.io/)

Libraries:
--------------------------------------
- [html5shiv](http://google.com)
HTML5SHIV

- [es5-shim](http://google.com)
ES5-SHIM

- [DataTables](http://google.com)
Third-party table utility, with most of the required functionalities needed for this project already implemented. Since 
this library is already widely used, feature rich and easy to extend, I opted to use it in the project to save a lot of 
coding and pitfalls already done on it. Anyway, I took care to isolate and abstract it's implementation, in order to facilitate 
it's replacement, if ever necessary.



Frameworks:
--------------------------------------
Markup and Styling:
- [Bootstrap](http://google.com)
Bootstrap

Application:
Given the small size of this application, I opted to not use any javascript framework, such as Angular, Backbone, Knockout, etc...

>P.S.: This application is limiting the number of markers by configuration present in config.js file, althought it's not yet giving any feedback when data has been capped. 





The Test:
--------------------------------------

###Founders Map Quest

This challenge consists from three independent parts. Feel free to solve all of them or just pick one that is more into your proficiency. If you are a great Frontend Developer who also knows how to design and make great UX please do all of the challenges and astonish us with you great talent!

- Design challenge, meant for graphic designers or Frontend Developers with design skills.
- UX challenge, meant for UX designers or Frontend Developers with UX skills.
- Coders challenge, meant for Frontend Developers.

####Meet Bob

Bob is a 34 years old entrepreneur who has already a successful startups on his account and he wants to focus on more philanthropic projects. He has an idea that he would like people to present their startups or even fresh new ideas they try in their garage on a interactive map. By doing so they could find out other entrepreneurs and network together to form stronger teams. 

Bob would like to make it as easy as possible. People just put the data online in a very simple way and it all is refreshed instantly. 

(for the purpose of this exercise let's assume that the input is a sample CSV file which is attached to this quest and named ‘sample.csv’)

####Bob wants 
- take input data in CSV format from textarea field. Remember that first row will contain header information and column separator can be configurable from list: comma, semicolon or tab

- each row should represent single marker on map, but user should be able to select which columns contain latitude/longitude information or what fields might contain some details for geolocating point on map. 

- user should have a possibility to select a column for marker labels 

- input data also should be visualized in a table

- rich content in table like images/links should be viewable/clickable there. You can assume that links ending with a common image-like extensions should be shown as images in table

- there should be also option not to show specific rows on map

- user should be able to sort/filter data in table by any column

- remember not to overwhelm user and prepare solution that keep in mind UX/design common rules

- be responsive when possible

- work on modern browsers

- visually attractive


####Frontend challenge
Your challenge is to present your frontend skills by preparing working sample of a an app that should:

- compliance with professional standards of frontend work (correct way of handling css, html, proper use of other libraries)
 
- code quality and allowance for future enhancements

- be responsive

- visually attractive

- work on popular browsers
 

The sample project should be stored on your Github/Bitbucket repository. Please add readme file if needed. 

>Note to frontenders: Its important that you will prepare working sample that helps us to better understand your frontend development skills and your professional approach. Working sample is more valuable than unfinished awesome fireworks that throw 100 errors. You can use any library available out there, but you must prove that it’s usage was carefully thought through. 



- todo: on datatable - add support for reverse geocoding
- todo: create error handler
- todo: write tests
- todo: finish README file