/**
 * Created by Philipe on 14/03/2016.
 */
'use strict';
const gulp          = require("gulp");
const babel         = require("gulp-babel");
const clean         = require("gulp-clean");
const flatten       = require("gulp-flatten");
const gulpSequence  = require("gulp-sequence");
const jshint        = require("gulp-jshint");
const lazypipe      = require("lazypipe");
const minifyHtml    = require("gulp-htmlmin");
const server        = require("gulp-webserver");
const sourcemaps    = require("gulp-sourcemaps");
const stylish       = require("jshint-stylish");
const stylus        = require('gulp-stylus');
const uglify        = require("gulp-uglify");
const useref        = require("gulp-useref");
const watch         = require("gulp-watch");
const webpack       = require("webpack");
const webpackConfig = require('./webpack.config');
const WebpackDevServer  = require("webpack-dev-server");
const webpackStream     = require("webpack-stream");

gulp.task("build", gulpSequence( "cleanUp", "useref", ["minifyHtml", "buildScripts", "compileCss", "moveFonts"]) );

gulp.task("buildScripts", () => {
    return gulp.src("src/scripts/**/*.js")
        .pipe(babel())
        .pipe(webpackStream(webpackConfig))
        .pipe(gulp.dest('./www/scripts'));
});

gulp.task("cleanUp", () => gulp.src('www', {read: false}).pipe(clean({force: true})) );

gulp.task('compileCss', function () {
    return gulp.src('src/styles/**/*.styl')
        .pipe(sourcemaps.init())
        .pipe(stylus())
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('www/styles'));
});

gulp.task("default", ["devServer"] );

gulp.task("devServer", () => {

    new WebpackDevServer(webpack(webpackConfig), {
        contentBase: "./src",
        publicPath: "/scripts/",
        hot: true
    }).listen(5000, "localhost", (err) => {
        if(err) console.log(err);
    });
});

gulp.task("lint", () => gulp.src("src/scripts/**/*.js").pipe(jshint()).pipe(jshint.reporter(stylish)) );

gulp.task("minifyHtml", () => {
    return gulp.src('www/**/*.html')
        .pipe(minifyHtml({
            removeStyleLinkTypeAttributes: true,
            removeScriptTypeAttributes: true,
            collapseInlineTagWhitespace: true,
            collapseWhitespace: true
        }))
        .pipe(gulp.dest('www'));
});

gulp.task("moveFonts", () => gulp.src("src/**/dist/**/*.{eot,svg,ttf,woff,woff2}").pipe(flatten()).pipe(gulp.dest("./www/fonts")) );

gulp.task("server", () => {
    var serverOptions = {
        livereload: true,
        directoryListing: false,
        open: false,
        host: 'localhost',
        port: 5001,
        https: false
    };
    gulp.src( 'www' )
        .pipe( server( serverOptions ) );
} );

gulp.task("useref", () => {
    return gulp.src('src/**/*.html')
        .pipe(useref({},
            lazypipe().pipe(sourcemaps.init, { loadMaps: true })
        ))
        .pipe(sourcemaps.write('maps'))
        .pipe(gulp.dest('www'));
});

gulp.task("watch", [ 'server' ], () => {
    gulp.watch( 'src/**/*.js', [ 'buildScripts' ] );
    gulp.watch( 'src/**/*.html', [ 'minifyHtml' ] );
});