/**
 * Created by Philipe on 29/03/2016.
 */
'use strict';
const path = require("path");
const ProgressBarPlugin = require('progress-bar-webpack-plugin');
const webpack = require("webpack");

module.exports = {
    entry: './src/scripts/app.js',
    output: {
        path: path.resolve(__dirname, "www"),
        filename: 'app.js'
    },
    module: {
        loaders: [
            {
                test: /\.js$/,
                loader: 'babel-loader',
                query: {
                    presets: ['es2015']
                }
            }
        ]
    },
    resolve: {
        extensions: ['', '.js', '.json']
    },
    devtool: "source-map",
    plugins: [
        new webpack.optimize.UglifyJsPlugin(),
        new webpack.HotModuleReplacementPlugin(),
        new ProgressBarPlugin()
    ]
};